<?php
/**
 * Created by PhpStorm.
 * User: Kezo
 * Date: 6/13/2018
 * Time: 10:16 AM
 */

include 'conexion.php';

if( (isset($_GET['usuarioM']) && !empty($_GET['usuarioM']) &&
    isset($_GET['contrasenaM']) && !empty($_GET['contrasenaM']) &&
    isset($_GET['ipM']) && !empty($_GET['ipM']) &&
    isset($_GET['puertoM']) && !empty($_GET['puertoM']) &&
    isset($_GET['baseDatosM']) && !empty($_GET['baseDatosM'])) ||
    (isset($_GET['usuarioE']) && !empty($_GET['usuarioE']) &&
    isset($_GET['contrasenaE']) && !empty($_GET['contrasenaE']) &&
    isset($_GET['ipE']) && !empty($_GET['ipE']) &&
    isset($_GET['puertoE']) && !empty($_GET['puertoE']) &&
    isset($_GET['baseDatosE']) && !empty($_GET['baseDatosE'])) &&
    isset($_GET['tipo']) && !empty($_GET['tipo']))
{

    //Codigo de consultas=========================================
    if($_GET['tipo'] == "create1"){
        $dbM = connection($_GET['ipM'],$_GET['puertoM'],$_GET['usuarioM'],$_GET['contrasenaM'],$_GET['baseDatosM']);
        createProcedureReplicar($dbM);
        close_connection($dbM);
    }elseif ($_GET['tipo'] == "create2"){
        $dbE = connection($_GET['ipE'],$_GET['puertoE'],$_GET['usuarioE'],$_GET['contrasenaE'],$_GET['baseDatosE']);
        $arrayError = array("estado" => false,
            "alerta" => "warning",
            "msg" => "Error query: ");
        $result = pg_query($dbE, "Do $$ begin if (not exists(SELECT * FROM  pg_catalog.pg_proc WHERE  proname  = 'dblink' limit 1)) then create extension dblink;".
            " end if; end $$; create or replace function areyougonnacarrythatweight(nombreTable text, justletmedie text, ".
            "ip text, puerto text, usuario text, passwo text, base text) ".
            "returns table(pass boolean) as $$ begin execute format ('insert into %s (select * from ".
            "dblink(''host='|| ip ||' port='|| puerto ||' user='|| usuario  ||' password='|| passwo ||' dbname='|| base ||''', ''select * from %s'') as %s)',".
            "nombreTable,nombreTable,justletmedie); end; $$ language plpgsql;")
        or die("*".json_encode($arrayError));
        close_connection($dbE);


    } elseif ($_GET['tipo'] == "reply"){
        $dbM = connection($_GET['ipM'],$_GET['puertoM'],$_GET['usuarioM'],$_GET['contrasenaM'],$_GET['baseDatosM']);
        $dbE = connection($_GET['ipE'],$_GET['puertoE'],$_GET['usuarioE'],$_GET['contrasenaE'],$_GET['baseDatosE']);
        if($_GET['ejec'] == 1){
            replicarEsquema($dbM);
        }elseif ($_GET['ejec'] == 2){
            $justletmidie = columnsInfo($dbM);
            replicarTabla($dbM, $dbE, $justletmidie);
        }elseif ($_GET['ejec'] == 3){
            replicarVista($dbM);
        }
        close_connection($dbM);
        close_connection($dbE);
    }
    //..Codigo de consultas=======================================

}
//Si los campos estan vacio devuelve este json_mensaje
else {
    $arrayError = array("estado" => false,
        "alerta" => "danger",
        "msg" => "Faltan datos para replicar las tablas");
    echo "*".json_encode($arrayError);
}

//=======================================================
function createProcedureReplicar($db){
    $query =    "Do $$ begin if (not exists(SELECT * FROM   pg_catalog.pg_proc WHERE  proname  = 'dblink' limit 1)) then ".
                "create extension dblink; end if; end $$;".

                " create or replace function justuClonesDeSombra(nombre varchar(50), vtipo int, ip varchar(50), puerto varchar(10), usuario varchar(50), pass varchar(10), base varchar(30)) returns table(".
                "columna varchar(50),tipo varchar(100)) as $$ DECLARE  teibolera record; traeselomnitrix record; mevalevergalaverga text; putabida int; BEGIN ".
                "if (vtipo = 1) then select dblink('host='|| ip ||' port='|| puerto ||' user='|| usuario  ||' password='|| pass ||' dbname='|| base ||'','create schema '||nombre);".
                "elsif (vtipo = 2) then CREATE TEMP TABLE teibolTemp(columna varchar(50),tipo varchar(100));".
                "FOR teibolera IN(select column_name,data_type from information_schema.columns where table_name = nombre) LOOP ".
                "insert into teibolTemp values (teibolera.column_name,teibolera.data_type); RETURN NEXT; END LOOP; putabida = 1; mevalevergalaverga = '';".
                "FOR traeselomnitrix IN(select column_name,data_type from information_schema.columns where table_name = nombre) ".
                "LOOP if (1 = (select count(*) from teibolTemp)) then mevalevergalaverga = mevalevergalaverga||'('||traeselomnitrix.column_name||' '||traeselomnitrix.data_type ||')';".
                "elsif (1 = putabida) then mevalevergalaverga = mevalevergalaverga||'('||traeselomnitrix.column_name||' '||traeselomnitrix.data_type ||',';".
                "elsif ((select count(*) from teibolTemp) > putabida) then mevalevergalaverga = mevalevergalaverga||traeselomnitrix.column_name||' '||traeselomnitrix.data_type ||',';".
                "else mevalevergalaverga = mevalevergalaverga||traeselomnitrix.column_name||' '||traeselomnitrix.data_type ||')'; end if;".
                "putabida = putabida + 1; RETURN NEXT; END LOOP; perform dblink('host='|| ip ||' port='|| puerto ||' user='|| usuario  ||' password='|| pass ||' dbname='|| base ||'',".
                "'create table '||nombre||'()'); FOR teibolera IN(select * from teibolTemp) LOOP ".
                "perform dblink('host='|| ip ||' port='|| puerto ||' user='|| usuario  ||' password='|| pass ||' dbname='|| base ||'',".
                "'alter table '||nombre||' add column '||teibolera.columna||' '||teibolera.tipo||''); RETURN NEXT; END LOOP; drop table teiboltemp;".
                "elsif (vtipo = 3) then CREATE TEMP TABLE teibolTemp (columna varchar(50),tipo varchar(100)); FOR teibolera IN(select column_name,data_type".
                " from information_schema.columns where table_name = nombre) LOOP insert into teibolTemp values (teibolera.column_name,teibolera.data_type);".
                "RETURN NEXT; END LOOP; putabida = 1; mevalevergalaverga = '';FOR traeselomnitrix IN(select column_name,data_type from information_schema.columns".
                " where table_name = nombre) LOOP if (1 = (select count(*) from teibolTemp)) then mevalevergalaverga = mevalevergalaverga||traeselomnitrix.column_name;".
                "elsif ((select count(*) from teibolTemp) > putabida)	then mevalevergalaverga = mevalevergalaverga||traeselomnitrix.column_name||',';".
                "else mevalevergalaverga = mevalevergalaverga||traeselomnitrix.column_name; end if; putabida = putabida + 1; RETURN NEXT;".
                " END LOOP; execute format('select dblink_exec(''host='|| ip ||' port='|| puerto ||' user='|| usuario ||' password='|| pass ||' dbname='|| base ||''',".
                "''create view %s_view as select %s from %s'')',nombre,mevalevergalaverga,nombre); else raise exception 'c mamo'; end if; end; $$ language plpgsql;".

                "create or replace function posmemato(nombre text) returns table(mevalevergalavergav2 text) as $$ declare teibolera record; ".
                "traeselomnitrix record; mevalevergalaverga text; putabida int; begin CREATE TEMP TABLE teibolTemp2 ( columna varchar(50), tipo varchar(100));".
                " FOR teibolera IN(select column_name,data_type  from information_schema.columns 	where table_name = nombre) LOOP ".
                "insert into teibolTemp2 values (teibolera.column_name,teibolera.data_type); RETURN NEXT; END LOOP; putabida = 1;".
                " mevalevergalaverga = '';FOR traeselomnitrix IN(select column_name,data_type from information_schema.columns where table_name = nombre) LOOP ".
                "if (1 = (select count(*) from teibolTemp2)) then mevalevergalaverga = mevalevergalaverga||'('||traeselomnitrix.column_name||' '||traeselomnitrix.data_type ||')';".
                " elsif (1 = putabida)	then mevalevergalaverga = mevalevergalaverga||'('||traeselomnitrix.column_name||' '||traeselomnitrix.data_type ||',';".
                "elsif ((select count(*) from teibolTemp2) > putabida)	then mevalevergalaverga = mevalevergalaverga||traeselomnitrix.column_name||' '||traeselomnitrix.data_type ||',';".
                "else mevalevergalaverga = mevalevergalaverga||traeselomnitrix.column_name||' '||traeselomnitrix.data_type ||')'; end if; ".
                "putabida = putabida + 1; RETURN NEXT; END LOOP; drop table teibolTemp2; return query select mevalevergalaverga; END; $$ LANGUAGE plpgsql;".

                "CREATE or REPLACE FUNCTION replica_tablas()".
                " RETURNS trigger AS $$ DECLARE    sql VARCHAR(500); json JSON; key text; query text; columns text;".
                "data text; ref cursor for select * from json_object_keys(row_to_json(new)); BEGIN CASE TG_OP WHEN 'INSERT' THEN columns=''; data=''; ".
                "json=row_to_json(new); OPEN ref; FETCH NEXT FROM ref into key; WHILE (FOUND) LOOP columns=columns||key||','; data=data||''''||(json->>key)||''','; ".
                "FETCH NEXT FROM ref into key; END LOOP; columns=substring(columns,0,length(columns)); data=substring(data,0,length(data)); ".
                "query=format('insert into %s (%s) values (%s);', TG_TABLE_NAME,columns,data);".
                " perform dblink('select dblink_exec(''host='|| TG_ARGV[1] ||' port='|| TG_ARGV[2] ||' user='|| TG_ARGV[3] ||' password='|| TG_ARGV[4] ||' dbname='|| TG_ARGV[5] ||'', query); ".
                "raise notice '%',query; WHEN 'DELETE' THEN query=format('delete from %s',TG_TABLE_NAME); ".
                "perform dblink('select dblink_exec(''host='|| TG_ARGV[1] ||' port='|| TG_ARGV[2] ||' user='|| TG_ARGV[3] ||' password='|| TG_ARGV[4] ||' dbname='|| TG_ARGV[5] ||'', query);".
                " raise notice '%',query; END CASE;  close ref; RETURN NEW; END; $$ LANGUAGE plpgsql;".

                "CREATE or REPLACE FUNCTION generaTrigger(tabla varchar(100), ip varchar(50), puerto varchar(10), usuario varchar(50), pass varchar(10), base varchar(30)) RETURNS void AS $$ begin ".
                "execute format('CREATE TRIGGER trg_trigger_%s AFTER insert or delete ON %s FOR EACH ROW EXECUTE PROCEDURE replica_tablas();', ".
                " tabla, tabla); end; $$ language plpgsql;";

    //Si sucede un error retorna este json
    $arrayError = array("estado" => false,
        "alerta" => "warning",
        "msg" => "Error query: ");
    $result = pg_query($db, $query)
    or die("*".json_encode($arrayError));

    $arrayResult = array("estado" => true,
        "alerta" => "success",
        "msg" => " Se creó el procedure de replicación en la base de datos ".$_GET['baseDatosE']." correctamente.");
    echo "*".json_encode($arrayResult);
}

//=======================================================
function columnsInfo($db){
    $arrayError = array("estado" => false,
        "alerta" => "warning",
        "msg" => "Error query: ");
    $result = pg_query($db, "select posmemato('".$_GET['tabla']."') order by posmemato asc limit 1")
    or die("*".json_encode($arrayError));
    return pg_fetch_row($result)[0];
}
//=======================================================
function replicarTabla($dbM, $dbE, $justletmedie){

    $arrayError = array("estado" => false,
        "alerta" => "warning",
        "msg" => "Error query: ");
    $result = pg_query($dbM, "select justuClonesDeSombra('".$_GET['tabla']."', ".$_GET['ejec'].", '".$_GET['ipE']."', '".$_GET['puertoE']."', '".$_GET['usuarioE']."', '".$_GET['contrasenaE']."', '".$_GET['baseDatosE']."')")
    or die("*".json_encode($arrayError));

    $arrayError = array("estado" => false,
        "alerta" => "warning",
        "msg" => "Error query: ");
    $result = pg_query($dbE, "select areyougonnacarrythatweight('".$_GET['tabla']."','".$justletmedie."', ".
        "'".$_GET['ipM']."', '".$_GET['puertoM']."', '".$_GET['usuarioM']."', '".$_GET['contrasenaM']."', '".$_GET['baseDatosM']."')")
    or die("*".json_encode($arrayError));

    $arrayError = array("estado" => false,
        "alerta" => "warning",
        "msg" => "Error query: ");
    $result = pg_query($dbM, "select generaTrigger('".$_GET['tabla']."', '".$_GET['ipE']."', '".$_GET['puertoE']."', ".
        " '".$_GET['usuarioE']."', '".$_GET['contrasenaE']."', '".$_GET['baseDatosE']."')")
    or die("*".json_encode($arrayError));
    ;

    $arrayResult = array("estado" => true,
        "alerta" => "success",
        "msg" =>  "Se replicó la tabla ".$_GET['tabla']." en la base de datos ".$_GET['baseDatosE']." correctamente.");
    echo "*".json_encode($arrayResult);
}

function replicarEsquema($dbM){
    $arrayResult = array("estado" => true,
        "alerta" => "success",
        "msg" =>  "Se replicó el esquema ".$_GET['tabla']." en la base de datos ".$_GET['baseDatosE']." correctamente.");
    $result = pg_query($dbM, "select justuclonesdesombra('".$_GET['tabla']."', ".$_GET['ejec'].", '".$_GET['ipE']."', '".$_GET['puertoE']."', '".$_GET['usuarioE']."', '".$_GET['contrasenaE']."', '".$_GET['baseDatosE']."')")
    or die("*".json_encode($arrayResult));

    echo "*".json_encode($arrayResult);
}

function replicarVista($dbM){
    $arrayError = array("estado" => false,
        "alerta" => "warning",
        "msg" => "Error query: ");
    $view = explode("_", $_GET['tabla']);
    $result = pg_query($dbM, "select justuClonesDeSombra('".$view[0]."', ".$_GET['ejec'].", '".$_GET['ipE']."', '".$_GET['puertoE']."', '".$_GET['usuarioE']."', '".$_GET['contrasenaE']."', '".$_GET['baseDatosE']."')")
    or die("*".json_encode($arrayError));

    $arrayResult = array("estado" => true,
        "alerta" => "success",
        "msg" =>  "Se replicó el esquema ".$_GET['tabla']." en la base de datos ".$_GET['baseDatosE']." correctamente.");
    echo "*".json_encode($arrayResult);
}

