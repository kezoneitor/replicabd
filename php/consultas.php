<?php 
include 'conexion.php';


if( isset($_GET['usuario']) && !empty($_GET['usuario']) &&
    isset($_GET['contrasena']) && !empty($_GET['contrasena']) &&
    isset($_GET['ip']) && !empty($_GET['ip']) &&
    isset($_GET['puerto']) && !empty($_GET['puerto']) &&
    isset($_GET['baseDatos']) && !empty($_GET['baseDatos']))
{
    $db = connection($_GET['ip'],$_GET['puerto'],$_GET['usuario'],$_GET['contrasena'],$_GET['baseDatos']);

    //Codigo de consultas=========================================
    if($_GET['tipo'] == 1){
        getEsquemas($db);
    }
    elseif ($_GET['tipo'] == 2){
        getBaseExiste();
    }
    //..Codigo de consultas=======================================

    close_connection($db);
}
//Si los campos estan vacio devuelve este json_mensaje
else {
    $arrayError = array("estado" => false,
        "alerta" => "danger",
        "msg" => "Introduzca todos los datos correspondientes");
    echo "*".json_encode($arrayError);
}

//Crear un html tabla para la parte de la tabla a replicar
function createhtmlTable($schemas, $tables, $views){
    $html = "<table class='table'><tbody><tr><td>".
            "<table class='table table-bordered'><thead>".
            "<tr><th>Esquemas</th><th><input type='checkbox' id='parentChecksSch' onclick=\"checkedall('schemaschk', 'parentChecksSch')\"></th></tr></thead><tbody>";
    while($rowSc = pg_fetch_row($schemas)){
        $html.= "<tr><td>". $rowSc[0] ."</td><td><input name='schemaschk' type='checkbox' value='". $rowSc[0] ."'></td></tr>";
    }
    $html .= "</tbody></table></td><td>".
             "<table class='table table-bordered'><thead>".
             "<tr><th>Tablas</th><th><input type='checkbox' id='parentChecksTab' onclick=\"checkedall('tableschk', 'parentChecksTab')\"></th></tr></thead><tbody>";
    while($rowTa = pg_fetch_row($tables)){
        $html.= "<tr><td>". $rowTa[0] ."</td><td><input name='tableschk' type='checkbox' value='". $rowTa[0] ."'></td></tr>";
    }
    $html .= "</tbody></table></td><td>".
        "<table class='table table-bordered'><thead>".
        "<tr><th>Vistas</th><th><input type='checkbox' id='parentChecksVis' onclick=\"checkedall('viewschk', 'parentChecksVis')\"></th></tr></thead><tbody>";
    while($rowVi = pg_fetch_row($views)){
        $html.= "<tr><td>". $rowVi[0] ."</td><td><input name='viewschk' type='checkbox' value='". $rowVi[0] ."'></td></tr>";
    }
    $html .= "</tbody></table></td></tr></tbody></table>";
    return $html;
}

//Consulta para tener la tabla de esquemas, tablas y vistas
function getEsquemas($db){
    //Si sucede un error retorna este json
    $arrayError = array("estado" => false,
        "alerta" => "warning",
        "msg" => "Error query: ");
    $schemas = pg_query($db, "select schema_name from information_schema.schemata where".
                                    " schema_name != 'pg_catalog' and schema_name != 'information_schema'")
        or die("*".json_encode($arrayError));

    $tables = pg_query($db, "select table_name from information_schema.tables where table_type = 'BASE TABLE'".
                                    " and table_schema != 'pg_catalog' and table_schema != 'information_schema'")
        or die("*".json_encode($arrayError));

    $views = pg_query($db, "select table_name from information_schema.tables where table_type = 'VIEW'".
                                    " and table_schema != 'pg_catalog' and table_schema != 'information_schema'")
        or die("*".json_encode($arrayError));

    $htmlTable = createhtmlTable($schemas, $tables, $views);

    $arrayResult = array("estado" => true,
        "alerta" => "success",
        "msg" => "Se recuperó la información de la base de datos ".$_GET['baseDatos']." correctamente.",
        "datos" => $htmlTable);
    echo "*".json_encode($arrayResult);
}

//Consulta para verificar que existe  la base de datos a replicar
function getBaseExiste(){
    $arrayResult = array("estado" => true,
        "alerta" => "success",
        "msg" => "Se recuperó la información de la base de datos ".$_GET['baseDatos']." correctamente.");
    echo "*".json_encode($arrayResult);
}


?>