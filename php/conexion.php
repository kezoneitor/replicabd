<?php


function connection($host, $port, $user, $pass, $dbname)
{
    $arrayError = array("estado" => false,
        "alerta" => "warning",
        "msg" => "No se pudo conectar a la base: ".$dbname.", host: ".$host.":". $port." o no existe dicha base. Intentelo de nuevo");
    $connect = pg_connect("host=$host port=$port user=$user password=$pass dbname=$dbname")
    or die("*".json_encode($arrayError));
    return $connect;
}

function close_connection($connect)
{
    pg_close($connect);
}
?>