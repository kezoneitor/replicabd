<?php 
include 'conexion.php';

if( isset($_GET['usuario']) && !empty($_GET['usuario']) &&
    isset($_GET['contrasena']) && !empty($_GET['contrasena']) &&
    isset($_GET['ip']) && !empty($_GET['ip']) &&
    isset($_GET['puerto']) && !empty($_GET['puerto']) &&
    isset($_GET['nBaseDatos']) && !empty($_GET['nBaseDatos']) &&
    isset($_GET['baseDatos']) && !empty($_GET['baseDatos']))
{
    $db = connection($_GET['ip'],$_GET['puerto'],$_GET['usuario'],$_GET['contrasena'],$_GET['baseDatos']);

    //Codigo de consultas=========================================
    insertDB($db);
    //..Codigo de consultas=======================================

    close_connection($db);
}
//Si los campos estan vacio devuelve este json_mensaje
else {
    $arrayError = array("estado" => false,
        "alerta" => "danger",
        "msg" => "Introduzca todos los datos correspondientes para la inserción");
    echo "*".json_encode($arrayError);
}

function insertDB($db){
    //Si sucede un error retorna este json
    $arrayError = array("estado" => false,
        "alerta" => "warning",
        "msg" => "Error query: ");
    $result = pg_query($db, "CREATE DATABASE ". $_GET['nBaseDatos'])
    or die("*".json_encode($arrayError));

    $arrayResult = array("estado" => true,
        "alerta" => "success",
        "msg" => "Se creó la base de datos ". $_GET['nBseDatos']." correctamente.");
    echo "*".json_encode($arrayResult);
}

?>