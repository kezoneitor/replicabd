function getAjax(url,tipo,parametros,func)
{
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = func;
    xhttp.open(tipo, url,true);
    xhttp.send(parametros);
}

function changeAlerts(nivel, texto){
    var Alerts = document.getElementById("Alerts");
    var newAlert = document.createElement("DIV");
    newAlert.className = "alert alert-"+nivel+" alert-dismissible fade show fixed-top";
    newAlert.innerHTML = texto + "<button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button>";
    Alerts.appendChild(newAlert);
    document.getElementById("msg").innerHTML += "<br/>" + texto;
}

