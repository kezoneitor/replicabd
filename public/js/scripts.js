function defaultPag(){
    verificarReplica();
}
tipo = 0;
baseReplicando = {
    "ip": 1,
    "baseDatos": 1,
    "ready": false};
baseReplicar = {
    "ip": 2,
    "baseDatos": 2,
    "ready": false};


function getSelect(ip,puerto,usuario,contrasena,baseDatos){
    let url = "php/consultas.php?ip="+ip+"&puerto="+puerto+"&usuario="+usuario+"&contrasena="+contrasena+"&baseDatos="+baseDatos+"&tipo="+tipo;
    let func;
    if(tipo === 1){
        func = function () {
            if (this.readyState === 4 && this.status === 200) {
                console.log(this.responseText);
                var conv = this.responseText.split("*");
                var result = JSON.parse(conv[1]);
                if(!result.estado){
                    baseReplicando = {
                        "ip": 1,
                        "baseDatos": 1,
                        "ready": false};
                    changeAlerts(result.alerta, result.msg + conv[0]);
                }else {
                    baseReplicando = {
                        "ip": ip,
                        "puerto": puerto,
                        "usuario": usuario,
                        "contrasena": contrasena,
                        "baseDatos": baseDatos,
                        "ready": true
                    };
                    changeAlerts(result.alerta, result.msg);
                    document.getElementById('dbnameReplicando').innerText = baseDatos;
                    $("#selectDB").modal('hide');
                    let frontTable = document.getElementById('accordionEsquemas');
                    frontTable.innerHTML = result.datos;
                }
                procesosM();
            }
        };
    }
    else{
        func = function () {
            if (this.readyState === 4 && this.status === 200) {
                console.log(this.responseText);
                let conv = this.responseText.split("*");
                let result = JSON.parse(conv[1]);
                if(!result.estado){
                    baseReplicar = {
                        "ip": 2,
                        "baseDatos": 2,
                        "ready": false};
                    changeAlerts(result.alerta, result.msg + conv[0]);
                    document.getElementById('crearBase').setAttribute("class", "d-flex justify-content-center pt-3");
                    document.getElementById('baseExiste').setAttribute("class", "d-none justify-content-center pt-3");
                    $("#selectDB").modal('hide');
                }else {
                    baseReplicar = {
                        "ip": ip,
                        "puerto": puerto,
                        "usuario": usuario,
                        "contrasena": contrasena,
                        "baseDatos": baseDatos,
                        "ready": true
                    };
                    changeAlerts(result.alerta, result.msg);
                    document.getElementById('dbnameReplicar').innerText = baseDatos;
                    $("#selectDB").modal('hide');
                    document.getElementById('crearBase').setAttribute("class", "d-none justify-content-center pt-3");
                    document.getElementById('baseExiste').setAttribute("class", "d-flex justify-content-center pt-3");
                }
                procesosE();
            }
        };
    }
    getAjax(url,'GET', null, func);
}

function insertDB(ip,puerto,usuario,contrasena,baseDatos){
    let url = "php/insertar.php?ip="+ip+"&puerto="+puerto+"&usuario="+usuario+"&contrasena="+contrasena+"&nBaseDatos="+baseDatos+"&baseDatos=postgres";
    let func = function () {
        if (this.readyState === 4 && this.status === 200) {
            console.log(this.responseText);
            let conv = this.responseText.split("*");
            let result = JSON.parse(conv[1]);
            if(!result.estado){
                baseReplicar = {
                    "ip": 2,
                    "baseDatos": 2,
                    "ready": false};
                changeAlerts(result.alerta, result.msg + conv[0]);
                document.getElementById('crearBase').setAttribute("class", "d-flex justify-content-center pt-3");
                document.getElementById('baseExiste').setAttribute("class", "d-none justify-content-center pt-3");
                $("#selectDB").modal('hide');
            }else {
                baseReplicar = {
                    "ip": ip,
                    "puerto": puerto,
                    "usuario": usuario,
                    "contrasena": contrasena,
                    "baseDatos": baseDatos,
                    "ready": true
                };
                changeAlerts(result.alerta, result.msg);
                document.getElementById('dbnameReplicar').innerText = baseDatos;
                $("#selectDB").modal('hide');
                document.getElementById('crearBase').setAttribute("class", "d-none justify-content-center pt-3");
                document.getElementById('baseExiste').setAttribute("class", "d-flex justify-content-center pt-3");
            }
        }
        procesosE();
    };
    getAjax(url,'GET', null, func);
}

function limpiar(form, t){
    tipo = t;
    let elementsForm = document.getElementsByName(form);
    elementsForm.forEach(function(element) {
        element.value = "";
    });
}

function checkedall(chkName, chkId){
    let checks = document.getElementsByName(chkName);
    let parentCheck = document.getElementById(chkId);
    checks.forEach(function(check) {
        check.checked = parentCheck.checked;
    });
}

totalProcesos = 2;
procesosRealizados = 0;
porcentaje = 0;
function replicando(){
    procesosRealizados = 0;
    porcentaje = 0;
    var elem = document.getElementById("myBar");
    var id = setInterval(frame, 100);
    function frame() {
        if (porcentaje >= 100) {
            document.getElementById("procesosEjecutados").innerHTML = "Proceso finalizado correctamente";
            document.getElementById("msg").innerHTML += "<br/> Proceso finalizado correctamente";
            clearInterval(id);
        } else {
            porcentaje = (procesosRealizados * 200) / totalProcesos;
            elem.style.width = porcentaje + '%';
            document.getElementById("label").innerHTML = porcentaje * 1 + '%';
        }
    }
}

function procesosM(){
    let url = "php/replicar.php?ipM="+baseReplicando.ip+"&puertoM="+baseReplicando.puerto+"&usuarioM="+baseReplicando.usuario+
        "&contrasenaM="+baseReplicando.contrasena+"&baseDatosM="+baseReplicando.baseDatos+
        "&ipE="+baseReplicar.ip+"&puertoE="+baseReplicar.puerto+"&usuarioE="+baseReplicar.usuario+
        "&contrasenaE="+baseReplicar.contrasena+"&baseDatosE="+baseReplicar.baseDatos;
    let urlpro = url + "&tipo=create1";
    let func = function () {
        if (this.readyState === 4 && this.status === 200) {
            console.log(this.responseText);
            var conv = this.responseText.split("*");
            var result = JSON.parse(conv[1]);
            if(!result.estado){
                changeAlerts(result.alerta, result.msg + conv[0]);
            }else {
                document.getElementById("msg").innerHTML += "<br/>"+ result.msg;
            }
        }
    };
    getAjax(urlpro,'GET', null, func);
}

function procesosE(){
    let url = "php/replicar.php?ipM="+baseReplicando.ip+"&puertoM="+baseReplicando.puerto+"&usuarioM="+baseReplicando.usuario+
        "&contrasenaM="+baseReplicando.contrasena+"&baseDatosM="+baseReplicando.baseDatos+
        "&ipE="+baseReplicar.ip+"&puertoE="+baseReplicar.puerto+"&usuarioE="+baseReplicar.usuario+
        "&contrasenaE="+baseReplicar.contrasena+"&baseDatosE="+baseReplicar.baseDatos;
    let urlpro = url + "&tipo=create2";
    let func = function () {
        if (this.readyState === 4 && this.status === 200) {
            console.log(this.responseText);
            var conv = this.responseText.split("*");
            var result = JSON.parse(conv[1]);
            if(!result.estado){
                changeAlerts(result.alerta, result.msg + conv[0]);
            }else {
                document.getElementById("msg").innerHTML += "<br/>"+ result.msg;
            }
        }
    };
    getAjax(urlpro,'GET', null, func);
}

function replicarBase(){
    totalProcesos = 2;
    procesosRealizados = 2;
    let url = "php/replicar.php?ipM="+baseReplicando.ip+"&puertoM="+baseReplicando.puerto+"&usuarioM="+baseReplicando.usuario+
        "&contrasenaM="+baseReplicando.contrasena+"&baseDatosM="+baseReplicando.baseDatos+
        "&ipE="+baseReplicar.ip+"&puertoE="+baseReplicar.puerto+"&usuarioE="+baseReplicar.usuario+
        "&contrasenaE="+baseReplicar.contrasena+"&baseDatosE="+baseReplicar.baseDatos;
    // Obtener lista de replicación
    let replicar = [];
    let checksSc = document.getElementsByName('schemaschk');
    let checksTa = document.getElementsByName('tableschk');
    let checksVi = document.getElementsByName('viewschk');
    //Recorrer lista check esquemas
    checksSc.forEach(function(check) {
        if(check.checked) {
            replicar.push([check.value, 1]);
            totalProcesos++;
        }
    });
    //Recorrer lista check tablas
    checksTa.forEach(function(check) {
        if(check.checked) {
            replicar.push([check.value, 2]);
            totalProcesos++;
        }
    });
    //Recorrer lista check vistas
    checksVi.forEach(function(check) {
        if(check.checked) {
            replicar.push([check.value, 3]);
            totalProcesos++;
        }
    });
    if(replicar.length > 0){
        replicando();
        //Mensaje
        document.getElementById("procesosEjecutados").innerHTML = "Obteniendo lista de tablas, vistas y esquemas para la replicación.";
        replicar.forEach(function (replica) {
            let urlrep = url +"&tabla="+replica[0]+"&tipo=reply"+"&ejec="+replica[1];
            let func = function () {
                if (this.readyState === 4 && this.status === 200) {
                    console.log(this.responseText);
                    var conv = this.responseText.split("*");
                    var result = JSON.parse(conv[1]);
                    if(!result.estado){
                        changeAlerts(result.alerta, result.msg + conv[0]);
                        console.log(result.msg + conv[0]);
                    }else {
                        document.getElementById("procesosEjecutados").innerHTML = result.msg;
                        document.getElementById("msg").innerHTML += "<br/>"+ result.msg;
                        procesosRealizados++;
                    }
                }
            };
            getAjax(urlrep,'GET', null, func);
        });
    }else{
        document.getElementById("procesosEjecutados").innerHTML = "No hay tablas, vistas o esquemas que replicar.";
    }
}

function verificarReplica(){
    let verificar = setInterval(function () {
        if(baseReplicar.ready && baseReplicando.ready){
            document.getElementById('replica').removeAttribute("disabled");
            document.getElementById('replica').setAttribute("class", "btn btn-success");
        }else{
            document.getElementById('replica').setAttribute("disabled", null);
            document.getElementById('replica').setAttribute("class", "btn btn-secondary disabled");
        }
    if(baseReplicar.ip === baseReplicando.ip && baseReplicar.baseDatos === baseReplicando.baseDatos) {
        baseReplicar = {
            "ip": 2,
            "baseDatos": 2,
            "ready": false};
        changeAlerts("danger", "Esta utilizando la misma base de datos para replicar sus datos. Por favor utilice otra.");
        document.getElementById('dbnameReplicar').innerText = "Nombre de la Base";
        document.getElementById('crearBase').setAttribute("class", "d-none justify-content-center pt-3");
        document.getElementById('baseExiste').setAttribute("class", "d-none justify-content-center pt-3");
    }
    }, 500);
}

/*Dividir las tablas y vistas por esquemas
    alltable = [];
    esqTemp = result.datos[0].esquema;
    subEsq = {
        "titulo": esqTemp,
        "tablas": [],
        "vistas": []
    };
    result.datos.forEach(function (dato) {
        if (esqTemp !== dato.esquema) {
            alltable.push(subEsq);
            esqTemp = dato.esquema;
            subEsq = {
                "titulo": esqTemp,
                "tablas": [],
                "vistas": []
            };
        }
        if (dato.tipo === "BASE TABLE") {
            subEsq.tablas.push(dato.fila);
        } else if (dato.tipo === "VIEW") {
            subEsq.vistas.push(dato.fila);
        }
    });
    alltable.push(subEsq);
*/